#include "circle.h"

Circle::Circle(const double & _radius):radius(_radius)
{
    this->radius = _radius;
    this->area = this->radius * this->radius * M_PI ;
    this->ference = 2 * M_PI * this->radius;
}


void Circle::set_area(const double& _area)
{
    this->area = _area;
    this->radius = sqrt( (this->area / M_PI) );
    this->ference = 2 * M_PI * this->radius;

}

void Circle::set_radius(const double& _radius)
{
    this->radius = _radius;
    this->area = this->radius * this->radius * M_PI  ;
    this->ference = 2 * M_PI * this->radius;
}

void Circle::set_ference(const double& _ference)
{
    this->ference = _ference;
    this->radius = this->ference / (2 * M_PI);
    this->area = this->radius * this->radius * M_PI  ;

}
