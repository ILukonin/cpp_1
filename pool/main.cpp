#include <iostream>
#include "circle.h"


int main()
{
    Circle pool(3); //бассейн шириной 3 метра
    Circle road(4); // дорожка радиусом 3 + 1 метр
    double cost_road = 1000; // стоимость квадратного метра дороги
    double cost_fence = 2000; // стоимость метра ограды

    std::cout << "Стоимость ограды:  " << cost_fence * road.get_ference() << " р" << std::endl;
    std::cout << "Стоимость дорожки: " << cost_road * ( road.get_area() - pool.get_area() ) << " р" << std::endl;


    return 0;
}

