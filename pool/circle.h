#ifndef CIRCLE_H
#define CIRCLE_H

#include <math.h>


class Circle
{
public:
    Circle(const double & _radius); //строим круг на основе радиуса
    ~Circle() {}; //виртуальный деструктор, чтоб не заморачиваться

    void set_area(const double& area);
    void set_radius(const double& radius);
    void set_ference(const double& ference);



    inline double get_radius()  const {return radius;};
    inline double get_area()  const {return area;};
    inline double get_ference()  const {return ference;};


private:
    double radius; //радиус
    double ference; //длина окружности
    double area; // площадь

};

#endif // CIRCLE_H
